chrome.devtools.panels.create('KanColle Sniffer', 'icon.png', 'panel.html', function(panel){
    panel.onShown.addListener(function(panelWindow){
        _window = panelWindow;
		chrome.devtools.network.onRequestFinished.addListener(function(request){
			request.getContent(function(content, encoding) {
                var mainScope = _window.angular.element(_window.$('body')).scope();
                mainScope.responseCallback(request.request.url, content);
			});
		});
    });
    /*
    panel.onSearch.addListener(function(action, queryString){
    	if (action != 'performSearch' || queryString == '') return;
    	_window.$('#ship tbody tr').each(function(){
    		$(this).removeClass();
    		$(this).find('td').each(function(){
	    		if ($(this).text().indexOf(queryString) >= 0) {
	    			$(this).parent().addClass('warning');
	    		}
    		});
    	});
    });
	*/
});
