KanColle Sniffer
===========

ブラウザゲーム [艦隊コレクション][kancolle top] 用のChrome拡張機能です

Version
---------

1.1.4

* APIアップデート対応(12/11)
* 装備情報をstackで表示

Feature
---------

* 艦娘一覧情報表示
* 遠征情報表示&通知
* 入渠情報表示&通知
* 建造情報表示&通知
* 資材情報表示

Usage
---------

1. ソースをダウンロード&解凍
2. Chromeの拡張機能([chrome://extensions/][extensions])を表示
3. デベロッパーモード にチェック
4. パッケージ化されていない拡張機能を読み込む... をクリック
5. 解凍したフォルダを選択
6. ツール > ディベロッパーツール を表示
7. ディベロッパーツールの KanColle Sniffer のパネルを表示
8. [艦隊コレクション][kancolle app] を起動

Note
---------

* APIの通信を監視し情報を表示しています
* 利用状況確認のため [Google Analytics] を使用しています
* ご利用は自己責任でお願いいたします

[kancolle top]: http://www.dmm.com/netgame/feature/kancolle.html
[kancolle app]: http://www.dmm.com/netgame/social/-/gadgets/=/app_id=854854/
[extensions]: chrome://extensions/
[Google Analytics]: http://www.google.co.jp/intl/ja/analytics/
