chrome.extension.onMessage.addListener(function (request, sender, sendResponse) {
	var id = request.id;
	var data = request.data;
	if (id == 'notifications') {
		chrome.notifications.create('', {
			type: 'basic',
			title: data.title,
			message: data.message,
			iconUrl: 'icon.png'
		}, function(notificationId){});
	}
});
