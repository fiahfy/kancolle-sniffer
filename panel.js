/**
 * Date Class
 */
Date.prototype.format = function(format){
	var r = {};
	r.Y = this.getFullYear()+'';
	r.n = this.getMonth()+1+'';
	r.j = this.getDate()+'';
	r.G = this.getHours()+'';
	r._i = this.getMinutes()+'';
	r._s = this.getSeconds()+'';

	r.y = r.Y.slice(-2);
	r.m = ('00'+r.n).slice(-2);
	r.d = ('00'+r.j).slice(-2);

	r.H = ('00'+r.G).slice(-2);
	r.i = ('00'+r._i).slice(-2);
	r.s = ('00'+r._s).slice(-2);

	for (var i in r)
	{
		format = format.replace(i, r[i])
	}

	return format;
}

Date.diffTime = function(from, to){
	return to.getTime()-from.getTime();
}
/*
Date.diffFormat = function(from, to, format){
	var diff = Date.diffTime(from, to);
	diff = diff > 0 ? diff : 0;
	diff /= 1000
	var d = {};
	d.G = parseInt(diff / (60*60))+'';
	d._i = parseInt((diff % (60*60)) / 60)+'';
	d._s = parseInt((diff % (60*60)) % 60)+'';
	d.H = ('00'+d.G).slice(-2);
	d.i = ('00'+d._i).slice(-2);
	d.s = ('00'+d._s).slice(-2);

	for (var i in d)
	{
		format = format.replace(i, d[i])
	}

	return format;
}*/

Date.diffFormat = function(diff, format){
	diff = diff > 0 ? diff : 0;
	diff /= 1000
	var d = {};
	d.G = parseInt(diff / (60*60))+'';
	d._i = parseInt((diff % (60*60)) / 60)+'';
	d._s = parseInt((diff % (60*60)) % 60)+'';
	d.H = d.G.length <= 1 ? ('00'+d.G).slice(-2) : d.G;
	d.i = ('00'+d._i).slice(-2);
	d.s = ('00'+d._s).slice(-2);

	for (var i in d)
	{
		format = format.replace(i, d[i])
	}

	return format;
}

/**
 * Object Class
 */
Object.defineProperty(Object.prototype, 'valueForKeyPath', {
	value: function(keyPath){
		if (keyPath.indexOf('.') >= 0) {
			key = keyPath.slice(0, keyPath.indexOf('.'));
			keyPath = keyPath.slice(keyPath.indexOf('.')+1);
			value = this[key];
			if (value) {
				return value.valueForKeyPath(keyPath);
			} else {
				return null;
			}
		} else {
			var value = this[keyPath];
			if (typeof(value) == 'undefined') {
				return null;
			}
			return value
		}
	},
	enumerable: false,
	writable: true,
	configurable: true
});

Object.defineProperty(Object.prototype, 'isArray', {
	value: function(){
		return Object.prototype.toString.call(this) === '[object Array]';
	},
	enumerable: false,
	writable: true,
	configurable: true
});

Object.defineProperty(Object.prototype, 'isObject', {
	value: function(){
		return Object.prototype.toString.call(this) === '[object Object]';
	},
	enumerable: false,
	writable: true,
	configurable: true
});

Object.defineProperty(Object.prototype, 'clone', {
	value: function(){
		if (this.isArray()) {
			return $.extend(true, [], this);
		} else if (this.isObject()) {
			return $.extend(true, {}, this);
		}
		return this;
	},
	enumerable: false,
	writable: true,
	configurable: true
});

/**
 * angularjs
 */
var app = angular.module('app', []);

/**
 * Directive
 */
app.directive('resize', function(){
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			$(element).css('width', 301);
			$('#left').css('width', $(window).width()-300);
			$(window).resize(function(){
				$('#left').width($(this).width()-$(element).width()-2);
				$(element).css('left', $('#left').width());
			});
			$(element).resizable({
				handles: 'w',
				start: function(e, ui){
				},
				resize: function(e, ui){
					$(this).width($(this).width()+1);
					$('#left').width($(window).width()-$(this).width());
				},
				stop: function(e, ui){
				}
			}).css('left', $('#left').width()-1);
		}
	};
});
app.directive('tooltip', function(){
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			$(element).attr('title', scope.$eval(attrs.tooltip)).tooltip();
		}
	};
});
app.directive('callback', function(){
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			scope.callback(scope, element, attrs);
		}
	};
});

/**
 * Main Controller
 */
app.controller('mainCtrl', ['$scope', function($scope){
	$scope.init = function(){
		// model
		$scope.ships = [];
		$scope.decks = [];
		$scope.ndocks = [];
		$scope.kdocks = [];
		$scope.material = [];
		$scope.items = [];

		// master
		$scope.master = $scope.loadStorage('master') || {};
		$scope.master.ships = $scope.master.ships || {};
		$scope.master.stypes = $scope.master.stypes || {};
		$scope.master.missions = $scope.master.missions || {};

		// config
		$scope.config = {};
		$scope.config.tab = 'ship';
		$scope.config.ships = $scope.config.ships || {};
		$scope.config.ships.sort = $scope.config.ships.sort || {column: 'deck', reverse: false};
		$scope.config.items = $scope.config.items || {};
		$scope.config.items.sort = $scope.config.items.sort || {column: 'name', reverse: false};
		$scope.config.missions = $scope.config.missions || {};
		$scope.config.ndocks = $scope.config.ndocks || {};
		$scope.config.kdocks = $scope.config.kdocks || {};

		// notification
		$scope.notification = {
			missions: {},
			ndocks: {},
			kdocks: {}
		};

		// callback interval
		$scope.callback = {
			missions: [],
			ndocks: [],
			kdocks: []
		};

		// setup
		angular.element(document).ready(function (){
			// 
			setInterval(function(){
				$scope.intervalCallback();
			}, 1000);

			// for debug
			if ($scope.debug) {
				angular.element($('head')).append(angular.element($('<script>')).attr('src', 'test.js'));
				test();
			}
		});
	};

	/**
	 * constant variables
	 */
	// debug mode
	$scope.debug = false;
	$scope.updateCallbacks = {
		'api_get_master/ship': function(data){
			$scope.updateMasterShips(data.api_data);
		},
		'api_get_master/stype': function(data){
			$scope.updateMasterStypes(data.api_data);
		},
		'api_get_master/mission': function(data){
			$scope.updateMasterMissions(data.api_data);
		},
		'api_get_member/ship': function(data){
			$scope.updateShips(data.api_data);
		},
		'api_get_member/ship2': function(data){
			$scope.updateDecks(data.api_data_deck);
			$scope.updateShips(data.api_data);
		},
		'api_get_member/ship3': function(data){
			$scope.updateDecks(data.api_data.api_deck_data);
			$scope.updateShips(data.api_data.api_ship_data);
		},
		'api_get_member/deck': function(data){
			$scope.updateDecks(data.api_data);
			$scope.updateShips($scope.ships);
		},
		'api_get_member/ndock': function(data){
			$scope.updateNDocks(data.api_data);
		},
		'api_get_member/kdock': function(data){
			$scope.updateKDocks(data.api_data);
		},
		'api_get_member/material': function(data){
			$scope.updateMaterial(data.api_data);
		},
		'api_get_member/slotitem': function(data){
			$scope.updateItems(data.api_data);
		}
	};

	$scope.refreshing = false;
	$scope.timer;
	$scope.refresh = function(){
		if ($scope.refreshing) {
			clearTimeout($scope.timer);
		}
		$scope.refreshing = true;
		$scope.timer = setTimeout(function(){
			$scope.$apply();
			$scope.refreshing = false;
		}, 500);
	};

	/**
	 * callback methods
	 */
	$scope.responseCallback = function(url, content){
		if (!content) return;

		var match = url.match(/^https?:\/\/[^\/]+\/kcsapi\/([^\/]+\/[^\/]+)\/?$/);
		if (!match) return;
		var method = match[1];

		if ($scope.debug) {
			console.log(url);
			console.log({1:content});
			console.log(JSON.parse(content.replace(/^svdata=/i, '')));
		}

		if ($scope.updateCallbacks[method]) {
			var data = JSON.parse(content.replace(/^svdata=/i, ''));
			$scope.updateCallbacks[method](data);
			$scope.refresh();
		}
	};
	$scope.intervalCallback = function(){
		for (var i in $scope.callback)
		{
			var callbacks = $scope.callback[i];
			for (var j in callbacks)
			{
				var callback = callbacks[j];
				callback();
			}
		}
	};
	/**
	 * local strage
	 */
	$scope.loadStorage = function(key){
		return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : null;
	};
	$scope.saveStorage = function(key, value){
		localStorage.setItem(key, JSON.stringify(value));
	};
	/**
	 * notifications
	 */
	$scope.notifications = function(data){
		chrome.extension.sendMessage({
			id: 'notifications',
			data: data
		}, function(response){});
	};
	/**
	 *
	 */
	$scope.getCondition = function(cond){
		if (isNaN(cond)) {
			return null;
		} else if (cond >= 0  && cond < 20) {
			return 'worst';
		} else if (cond < 30) {
			return 'worse';
		} else if (cond < 40) {
			return 'bad';
		} else if (cond < 50) {
			return 'normal';
		} else if (cond <= 100) {
			return 'good';
		}
		return null;
	};
	$scope.getRate = function(rate){
		if (rate >= 0 && rate <= 0.25) {
			return 'zero';
		} else if (rate <= 0.5) {
			return 'quarter';
		} else if (rate <= 0.75) {
			return 'half';
		} else if (rate < 1) {
			return 'three-quater';
		} else if (rate == 1) {
			return 'full';
		}
		return null;
	};
	$scope.getExp = function(level){
		if (level > 0 && level <= 51) {
			return (level-1)*100;
		} else if (level <= 61) {
			return (level-51)*200+5000;
		} else if (level <= 71) {
			return (level-61)*300+7000;
		} else if (level <= 81) {
			return (level-71)*400+10000;
		} else if (level <= 91) {
			return (level-81)*500+14000;
		} else if (level == 92) {
			return 20000;
		} else if (level == 93) {
			return 22000;
		} else if (level == 94) {
			return 25000;
		} else if (level == 95) {
			return 30000;
		} else if (level == 96) {
			return 40000;
		} else if (level == 97) {
			return 60000;
		} else if (level == 98) {
			return 90000;
		} else if (level == 99) {
			return 148500;
		}
		return 0;
	};
	$scope.getTotalExp = function(level){
		if (level <= 0) {
			return 0;
		}
		return $scope.getExp(level) + $scope.getTotalExp(level-1);
	};
	$scope.getSokuName = function(sokuh){
		if (sokuh == 0) {
			return '低速';
		} else if (sokuh == 1) {
			return '高速';
		}
		return null;
	};
	$scope.getLengName = function(leng){
		if (leng == 1) {
			return '短';
		} else if (leng == 2) {
			return '中';
		} else if (leng == 3) {
			return '長';
		} else if (leng == 4) {
			return '超長';
		}
		return null;
	};
	$scope.getRareName = function(rare){
		if (rare == 0) {
			return 'コモン';
		} else if (rare == 1) {
			return 'レア';
		} else if (rare == 2) {
			return 'ホロ';
		} else if (rare == 3) {
			return 'Sホロ';
		} else if (rare == 4) {
			return 'SSホロ';
		}
		return null;
	};
	$scope.getDateString = function(t, f){
		return (new Date(t)).format(f);
	};
	$scope.getRestString = function(t, f){
		return Date.diffFormat(t-(new Date()).getTime(), f);
	};
	/**
	 * update model
	 */
	$scope.updateMasterShips = function(data){
		for (var i in data)
		{
			var ship = data[i];
			$scope.master.ships[ship.api_id] = ship;
		}
		$scope.saveStorage('master', $scope.master);
	};
	$scope.updateMasterStypes = function(data){
		for (var i in data)
		{
			var stype = data[i];
			$scope.master.stypes[stype.api_id] = stype;
		}
		$scope.saveStorage('master', $scope.master);
	};
	$scope.updateMasterMissions = function(data){
		for (var i in data)
		{
			var mission = data[i];
			$scope.master.missions[mission.api_id] = mission;
		}
		$scope.saveStorage('master', $scope.master);
	};
	$scope.updateShips = function(data){
		$scope.ships = [];
		for (var i in data)
		{
			var ship = data[i];

			// hp
			ship.api_hp_rate = ship.api_nowhp/ship.api_maxhp;
			// exp
			ship.api_exp_now = ship.api_exp[0] - $scope.getTotalExp(ship.api_lv);
			ship.api_exp_max = $scope.getExp(ship.api_lv+1);
			ship.api_exp_rate = ship.api_exp_now/ship.api_exp_max;
			// ship
			if ($scope.master.ships[ship.api_ship_id]) {
				var s = $scope.master.ships[ship.api_ship_id];
				ship.api_ship = s.clone();
				// fuel, bull
				ship.api_fuel_rate = ship.api_fuel/s.api_fuel_max;
				ship.api_bull_rate = ship.api_bull/s.api_bull_max;
				// kyoka
				ship.api_kyouka_rest = [];
				ship.api_kyouka_rest[0] = s.api_houg[1] - s.api_houg[0] - ship.api_kyouka[0];
				ship.api_kyouka_rest[1] = s.api_raig[1] - s.api_raig[0] - ship.api_kyouka[1];
				ship.api_kyouka_rest[2] = s.api_tyku[1] - s.api_tyku[0] - ship.api_kyouka[2];
				ship.api_kyouka_rest[3] = s.api_souk[1] - s.api_souk[0] - ship.api_kyouka[3];
				ship.api_kyouka_rest_total = ship.api_kyouka_rest[0]+ship.api_kyouka_rest[1]+ship.api_kyouka_rest[2]+ship.api_kyouka_rest[3];
				// stype
				if ($scope.master.stypes[ship.api_ship.api_stype]) {
					var s = $scope.master.stypes[ship.api_ship.api_stype];
					ship.api_ship.api_stype = s.clone();
				}
			}
			// deck
			ship.api_deck = null;
			for (var i in $scope.decks)
			{
				var d = $scope.decks[i];
				if (d.api_ship.indexOf(ship.api_id) >= 0) {
					ship.api_deck = d.clone();
					ship.api_deck.api_index = d.api_ship.indexOf(ship.api_id);
					break;
				}
			}
			$scope.ships.push(ship);
		}
	};
	$scope.updateDecks = function(data){
		$scope.decks = [];
		for (var i in data)
		{
			var deck = data[i];
			// mission
			if ($scope.master.missions[deck.api_mission[1]]) {
				var m = $scope.master.missions[deck.api_mission[1]];
				deck.api_mission_master = m.clone();
			}
			$scope.decks.push(deck);
		}
		// clear
		$scope.callback.missions = [];
	};
	$scope.updateNDocks = function(data){
		$scope.ndocks = [];
		for (var i in data)
		{
			var ndock = data[i];
			// ship
			for (var j in $scope.ships) {
				var s = $scope.ships[j];
				if (ndock.api_ship_id == s.api_id) {
					ndock.api_ship = s.clone();
					break;
				}
			}
			$scope.ndocks.push(ndock);
		}
		// clear
		$scope.callback.ndocks = [];
	};
	$scope.updateKDocks = function(data){
		$scope.kdocks = [];
		for (var i in data)
		{
			var kdock = data[i];
			// ship
			if ($scope.master.ships[kdock.api_created_ship_id]) {
				var s = $scope.master.ships[kdock.api_created_ship_id];
				kdock.api_created_ship = s.clone();
				// stype
				if ($scope.master.stypes[kdock.api_created_ship.api_stype]) {
					var s = $scope.master.stypes[kdock.api_created_ship.api_stype];
					kdock.api_created_ship.api_stype = s.clone();
				}
			}
			$scope.kdocks.push(kdock);
		}
		// clear
		$scope.callback.kdocks = [];
	};
	$scope.updateMaterial = function(data){
		$scope.material = data;
	};
	$scope.updateItems = function(data){
		var hash = {};
		for (var i in data)
		{
			var item = data[i];
			if (hash[item.api_slotitem_id]) {
				hash[item.api_slotitem_id].api_stack++;
			} else {
				item.api_stack = 1;
				hash[item.api_slotitem_id] = item;
			}
		}
		$scope.items = [];
		for (var i in hash)
		{
			var item = hash[i];
			$scope.items.push(item);
		}
	};
	$scope.init();
}]);

/**
 * Controllers
 */
app.controller('shipsCtrl', ['$scope', function($scope){
	$scope.sort = function(column, toggle){
		var reverse = $scope.config.ships.sort.reverse;
		if (column != $scope.config.ships.sort.column) {
			reverse = false;
		} else if (toggle) {
			reverse = !reverse;
		}
		var orders = [];
		switch (column) {
			case 'index':
				orders = ['api_id'];
				break;
			case 'id':
				orders = ['-api_id'];
				break;
			case 'deck':
				orders = ['api_deck.api_id', 'api_deck.api_index'];
				break;
			case 'stype':
				orders = ['-api_ship.api_stype.api_id', 'api_ship.api_id', 'api_id'];
				break;
			case 'name':
				orders = ['api_ship.api_id', 'api_id'];
				break;
			case 'lv':
				orders = ['-api_lv', 'api_ship.api_id', 'api_id'];
				break;
			case 'exp':
				orders = ['-api_exp_rate', 'api_id'];
				break;
			case 'hp':
				orders = ['api_hp_rate', 'api_id'];
				break;
			case 'cond':
				orders = ['-api_cond', 'api_id'];
				break;
			case 'fuel':
				orders = ['api_fuel_rate', 'api_id'];
				break;
			case 'bull':
				orders = ['api_bull_rate', 'api_id'];
				break;
			case 'karyoku':
				orders = ['-api_karyoku.0', 'api_id'];
				break;
			case 'soukou':
				orders = ['-api_soukou.0', 'api_id'];
				break;
			case 'raisou':
				orders = ['-api_raisou.0', 'api_id'];
				break;
			case 'kaihi':
				orders = ['-api_kaihi.0', 'api_id'];
				break;
			case 'taiku':
				orders = ['-api_taiku.0', 'api_id'];
				break;
			case 'taisen':
				orders = ['-api_taisen.0', 'api_id'];
				break;
			case 'sakuteki':
				orders = ['-api_sakuteki.0', 'api_id'];
				break;
			case 'lucky':
				orders = ['-api_lucky.0', 'api_id'];
				break;
			case 'onslot':
				orders = ['-api_ship.api_tous.1', 'api_id'];
				break;
			case 'soku':
				orders = ['-api_ship.api_soku', 'api_id'];
				break;
			case 'leng':
				orders = ['-api_leng', 'api_id'];
				break;
			case 'kyouka':
				orders = ['api_kyouka_rest_total', 'api_id'];
				break;
			case 'kyouka_karyoku':
				orders = ['api_kyouka_rest.0', 'api_id'];
				break;
			case 'kyouka_raisou':
				orders = ['api_kyouka_rest.1', 'api_id'];
				break;
			case 'kyouka_taiku':
				orders = ['api_kyouka_rest.2', 'api_id'];
				break;
			case 'kyouka_soukou':
				orders = ['api_kyouka_rest.3', 'api_id'];
				break;
			default:
				break;
		}
		$scope.config.ships.sort.column = column;
		$scope.config.ships.sort.reverse = reverse;
		$scope.order = orders;
		$scope.reverse = reverse;
		$scope.current = column
	};
	$scope.sort($scope.config.ships.sort.column, false);
}]);

app.controller('itemsCtrl', ['$scope', function($scope){
	$scope.sort = function(column, toggle){
		var reverse = $scope.config.items.sort.reverse;
		if (column != $scope.config.items.sort.column) {
			reverse = false;
		} else if (toggle) {
			reverse = !reverse;
		}
		var orders = [];
		switch (column) {
			case 'index':
				orders = ['api_slotitem_id'];
				break;
			case 'name':
				orders = ['api_slotitem_id'];
				break;
			case 'stack':
				orders = ['-api_stack', 'api_slotitem_id'];
				break;
			case 'rare':
				orders = ['-api_rare', 'api_slotitem_id'];
				break;
			case 'houg':
				orders = ['-api_houg', 'api_slotitem_id'];
				break;
			case 'raig':
				orders = ['-api_raig', 'api_slotitem_id'];
				break;
			case 'baku':
				orders = ['-api_baku', 'api_slotitem_id'];
				break;
			case 'tyku':
				orders = ['-api_tyku', 'api_slotitem_id'];
				break;
			case 'tais':
				orders = ['-api_tais', 'api_slotitem_id'];
				break;
			case 'saku':
				orders = ['-api_saku', 'api_slotitem_id'];
				break;
			case 'houm':
				orders = ['-api_houm', 'api_slotitem_id'];
				break;
			default:
				break;
		}
		$scope.config.items.sort.column = column;
		$scope.config.items.sort.reverse = reverse;
		$scope.order = orders;
		$scope.reverse = reverse;
		$scope.current = column
	};
	$scope.sort($scope.config.items.sort.column, false);
}]);

app.controller('missionsCtrl', ['$scope', function($scope){
	$scope.callback = function(scope, element, attrs){
		var callback = function(){
			if (scope.deck.api_mission[0] == 0) return;

			var diff = scope.deck.api_mission[2] - (new Date()).getTime();
			$(element).text(Date.diffFormat(diff, 'H:i:s'));

			diff -= 60*1000;// minus 1 minute
			if (diff > 0) {
				$scope.notification.missions[scope.deck.api_id] = true;
			} else {
				if ($scope.notification.missions[scope.deck.api_id]) {
					$scope.notifications({
						id: scope.deck.api_id,
						title: '遠征が完了しました',
						message: scope.deck.api_name+' '+(scope.deck.valueForKeyPath('api_mission_master.api_name') || '-')
					});
					$scope.notification.missions[scope.deck.api_id] = false;
				}
			}
		};
		$scope.$parent.callback.missions.push(callback);
	};
}]);

app.controller('ndocksCtrl', ['$scope', function($scope){
	$scope.callback = function(scope, element, attrs){
		var callback = function(){
			if (scope.ndock.api_state <= 0) return;

			var diff = scope.ndock.api_complete_time - (new Date()).getTime();
			$(element).text(Date.diffFormat(diff, 'H:i:s'));

			diff -= 60*1000;// minus 1 minute
			if (diff > 0) {
				$scope.notification.ndocks[scope.ndock.api_id] = true;
			} else {
				if ($scope.notification.ndocks[scope.ndock.api_id]) {
					$scope.notifications({
						id: scope.ndock.api_id,
						title: '修理が完了しました',
						message: (scope.ndock.valueForKeyPath('api_ship.api_ship.api_stype.api_name') || '-')+' '+(scope.ndock.valueForKeyPath('api_ship.api_ship.api_name') || '-')
					});
					$scope.notification.ndocks[scope.ndock.api_id] = false;
				}
			}
		};
		$scope.$parent.callback.ndocks.push(callback);
	};
}]);

app.controller('kdocksCtrl', ['$scope', function($scope){
	$scope.callback = function(scope, element, attrs){
		var callback = function(){
			if (scope.kdock.api_state <= 0) return;

			var diff = scope.kdock.api_complete_time - (new Date()).getTime();
			$(element).text(Date.diffFormat(diff, 'H:i:s'));

			if (diff > 0) {
				$scope.notification.kdocks[scope.kdock.api_id] = true;
			} else {
				if ($scope.notification.kdocks[scope.kdock.api_id]) {
					$scope.notifications({
						id: scope.kdock.api_id,
						title: '建造が完了しました',
						message: (scope.kdock.valueForKeyPath('api_created_ship.api_stype.api_name') || '-')+' '+(scope.kdock.valueForKeyPath('api_created_ship.api_name') || '-')
					});
					$scope.notification.kdocks[scope.kdock.api_id] = false;
				}
			}
		};
		$scope.$parent.callback.kdocks.push(callback);
	};
}]);
